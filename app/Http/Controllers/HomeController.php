<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Traits\CaptchaTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;


class HomeController extends Controller
{

    use CaptchaTrait;


    public function index(){
        return view('welcome');
    }


    public function login(){
        return view('login');
    }

    public function postRegister(){

        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
            'passConfirm' => 'required|same:password',
            'g-recaptcha-response'  => 'required'

        ];
        $data = Input::all();
        // dd($data);

        $validator = Validator::make($data,$rules);

        if($validator->fails()){
            return redirect()->back()
                ->withErrors($validator)
                ->withInput(Input::except('password','passConfirm'));
        } else{
            $user = new User();
            $user->fill($data);
            $user->password = bcrypt(Input::get('password'));
            $user->save();
            return redirect('login');


        }

    }

    public function dashboard(){

        return view('index');

    }

    public function authenticate(Request $request){

        $rules = [
            'email' => 'required',
            'password' => 'required',
            'g-recaptcha-response'  => 'required'

        ];

        $data = $request->all();

        $userData = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        $validator = Validator::make($data,$rules);

        if($validator->fails()){
            return redirect()->back()
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }else{
            if(Auth::attempt($userData)){
                return redirect('dashboard');
            }else{

                return redirect()->back()
                    ->withErrors(['loginError'=>'Invalid Username/Password']);


            }
        }
    }

    public function logout(){
        Auth::logout();
return redirect('/');
    }


}
