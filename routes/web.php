<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','HomeController@index');
Route::post('register','HomeController@postRegister');
Route::get('login','HomeController@login');
Route::post('postlogin','HomeController@authenticate');
Route::get('dashboard','HomeController@dashboard');
Route::get('logout','HomeController@logout');